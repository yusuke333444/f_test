import path from "path";
import { program } from "commander";

import { readLog } from "../utils/log-reader";
import {
  getRecord3sMap,
  LogRecord,
  LogRecord3,
  convertLogToRecords,
} from "../utils/log-parser";
import {
  printAnomalousPeriods,
  Period,
  PeriodType,
  ServerInfo3,
} from "../utils/result-printer";

const isFailureEnd = (
  failureStartDate: Date | null,
  timeOutCount: number,
  allowedRetries: number
): failureStartDate is Date => {
  return failureStartDate != null && timeOutCount > allowedRetries;
};

const isEnd = (
  startDate: Date | null,
  timeOutCount: number,
  allowedRetries: number,
  type: PeriodType
): startDate is Date => {
  switch (type) {
    case "failure":
      return isFailureEnd(startDate, timeOutCount, allowedRetries);
    case "overloading":
      return startDate != null;
    default:
      const strange: never = type;
      throw new Error(`typeに${strange}を指定しないでください`);
  }
};

const isNormal = (record: LogRecord3, type: PeriodType, t?: number) => {
  switch (type) {
    case "failure":
      return !record.ping.timeOut;
    case "overloading":
      if (t == null) {
        throw new Error("過負荷状態の判定にはtを指定してください");
      }
      return record.meanResTime == null || record.meanResTime < t;
    default:
      const strange: never = type;
      throw new Error(`typeに${strange}を指定しないでください`);
  }
};

const calcPeriods = (
  records: LogRecord3[],
  type: PeriodType,
  allowedRetries = 0,
  t?: number
) => {
  if (records.every((record) => isNormal(record, type, t))) {
    return [];
  }

  let startDate: Date | null = null;
  let timeOutCount = 0;
  const periods: Period[] = [];
  records.forEach((record) => {
    if (!isNormal(record, type, t)) {
      timeOutCount += 1;
      if (startDate == null) {
        startDate = record.dateTime;
      }
      return;
    }

    if (isEnd(startDate, timeOutCount, allowedRetries, type)) {
      periods.push({
        start: startDate,
        end: record.dateTime,
        type,
      });
      startDate = null;
    }
    timeOutCount = 0;
  });

  if (isEnd(startDate, timeOutCount, allowedRetries, type)) {
    periods.push({ start: startDate, type });
  }

  return periods;
};

const calcFailurePeriods = (
  records: LogRecord3[],
  allowedRetries = 0
): Period[] => {
  return calcPeriods(records, "failure", allowedRetries);
};

const calcOverloadingPeriods = (records: LogRecord3[], t: number): Period[] => {
  return calcPeriods(records, "overloading", 0, t);
};

// サーバ毎の記録や故障期間、過負荷期間をまとめたマップを返却する
const getServerInfos = (
  records: LogRecord[],
  allowedRetries = 0,
  m: number,
  t: number
): Map<string, ServerInfo3> => {
  const record3sForEachDestination = getRecord3sMap(records, m);
  const serverInfoMap = new Map<string, ServerInfo3>();

  record3sForEachDestination.forEach((serverRecords, address) => {
    const failurePeriods = calcFailurePeriods(serverRecords, allowedRetries);
    const overloadingPeriods = calcOverloadingPeriods(serverRecords, t);

    serverInfoMap.set(address, {
      address,
      records: serverRecords,
      failurePeriods,
      overloadingPeriods,
    });
  });

  return serverInfoMap;
};

// 設問3を実行する
export const solveQuestion3 = async (
  filePath: string,
  N: number,
  m: number,
  t: number
) => {
  const textData = await readLog(filePath);
  const records = convertLogToRecords(textData);
  printAnomalousPeriods(getServerInfos(records, N - 1, m, t));
};

// 設問3を実行するコマンドを登録する
export const registerSolve3 = () => {
  program
    .command("solve3")
    .description("設問3のプログラムを実行します")
    .argument("[filePath]", "読み込むログのパス", "logs/sample.log")
    .option("-N, --num [number]", "何回タイムアウトしたら故障とみなすか", "2")
    .option("-m, --m [number]", "直近何回の平均応答時間を見るか", "3")
    .option("-t, --t [number]", "過負荷状態とみなす応答時間(ms)", "50")
    .action(async (filePath, options) => {
      try {
        const { num, m, t } = options;
        await solveQuestion3(path.resolve(filePath), +num, +m, +t);
      } catch {
        console.error("引数に指定したファイルは存在しません");
      }
    });
};
