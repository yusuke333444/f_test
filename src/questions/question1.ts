import path from "path";
import { program } from "commander";

import { readLog } from "../utils/log-reader";
import {
  getRecordsMap,
  LogRecord,
  convertLogToRecords,
} from "../utils/log-parser";
import {
  printFailurePeriods,
  Period,
  ServerInfo,
} from "../utils/result-printer";

// ログの情報から故障期間を導出する
const calcFailurePeriods = (records: LogRecord[]): Period[] => {
  if (records.every((record) => !record.ping.timeOut)) {
    return [];
  }

  let failureStartDate: Date | null = null;
  const failurePeriods: Period[] = [];
  records.forEach((record) => {
    if (record.ping.timeOut) {
      if (failureStartDate == null) {
        // 故障の開始
        failureStartDate = record.dateTime;
        return;
      }
      // 上記のIF文を通らない場合は故障が継続中
    }

    if (failureStartDate != null && !record.ping.timeOut) {
      // 故障の終了
      failurePeriods.push({
        start: failureStartDate,
        end: record.dateTime,
        type: "failure",
      });
      failureStartDate = null;
    }
  });

  if (failureStartDate != null) {
    // ログの最後まで故障が未終了
    failurePeriods.push({ start: failureStartDate, type: "failure" });
  }

  return failurePeriods;
};

// サーバ毎の記録や故障期間をまとめたマップを返却する
const getServerInfos = (records: LogRecord[]): Map<string, ServerInfo> => {
  const recordsForEachDestination = getRecordsMap(records);
  const serverInfoMap = new Map<string, ServerInfo>();

  recordsForEachDestination.forEach((serverRecords, address) => {
    const failurePeriods = calcFailurePeriods(serverRecords);
    serverInfoMap.set(address, {
      address,
      records: serverRecords,
      failurePeriods,
    });
  });

  return serverInfoMap;
};

// 設問1を実行する
export const solveQuestion1 = async (filePath: string) => {
  const textData = await readLog(filePath);
  const records = convertLogToRecords(textData);
  printFailurePeriods(getServerInfos(records));
};

// 設問1を実行するコマンドを登録する
export const registerSolve1 = () => {
  program
    .command("solve1")
    .description("設問1のプログラムを実行します")
    .argument("[filePath]", "読み込むログのパス", "logs/sample.log")
    .action(async (filePath: string) => {
      try {
        await solveQuestion1(path.resolve(filePath));
      } catch {
        console.error("引数に指定したファイルは存在しません");
      }
    });
};
