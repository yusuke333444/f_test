import path from "path";
import { program } from "commander";
import { isAfter, isBefore, isEqual } from "date-fns";

import { readLog } from "../utils/log-reader";
import { convertLogToRecords } from "../utils/log-parser";
import {
  Period,
  printSubnetFailurePeriods,
  ServerInfo,
  SubnetInfo,
} from "../utils/result-printer";
import { getServerInfos } from "./question2";

const toBinary = (address: string) => {
  const b = address
    .split(".")
    .map((section) => ("00000000" + (+section).toString(2)).slice(-8))
    .join("");
  return BigInt(parseInt(b, 2));
};

const getMask = (prefix: number) => {
  const b = Array(32).fill(0).fill(1, 0, prefix).join("");
  return BigInt(parseInt(b, 2));
};

// BigIntでないと32ビット目が1で論理和計算時に負数になってしまう
const getDecimalAddress = (binary: bigint): string => {
  const binStr = binary.toString(2);
  return [
    binStr.slice(0, 8),
    binStr.slice(8, 16),
    binStr.slice(16, 24),
    binStr.slice(24, 32),
  ]
    .map((section) => parseInt(section, 2))
    .join(".");
};

const calcSubnetAddress = (address: string, prefix: number) => {
  return getDecimalAddress(toBinary(address) & getMask(prefix));
};

const isBeforeEqualDate = (d1?: Date, d2?: Date) => {
  if (d2 == null) {
    return true;
  }

  if (d1 == null) {
    return false;
  }

  return isBefore(d1, d2 || isEqual(d1, d2));
};

const isAfterEqualDate = (d1?: Date, d2?: Date) => {
  if (d1 == null) {
    return true;
  }

  if (d2 == null) {
    return false;
  }

  return isAfter(d1, d2) || isEqual(d1, d2);
};

const calcPeriodOverlap = (p1: Period, p2: Period): Period | null => {
  const start = isAfter(p1.start, p2.start) ? p1.start : p2.start;
  if (
    isBeforeEqualDate(p1.end, p2.start) ||
    isBeforeEqualDate(p2.end, p1.start)
  ) {
    return null;
  }

  const end = isBeforeEqualDate(p1.end, p2.end) ? p1.end : p2.end;
  if (end == null) {
    return { start, type: "failure" };
  }
  return { start, end, type: "failure" };
};

const calcPeriodsOverlap = (
  periods1: Period[],
  periods2: Period[]
): Period[] => {
  const result: Period[] = [];
  periods1.forEach((p1) => {
    periods2.forEach((p2) => {
      const overlap = calcPeriodOverlap(p1, p2);
      if (overlap == null) {
        return;
      }
      result.push(overlap);
    });
  });
  return result;
};

const getSubnetInfos = (
  serverInfosMap: Map<string, ServerInfo>
): Map<string, SubnetInfo> => {
  const map = new Map<string, SubnetInfo>();
  serverInfosMap.forEach((serverInfo, address) => {
    const prefix = serverInfo.records[0].destination.prefix;
    const subnetAddress = calcSubnetAddress(address, prefix);
    if (map.has(subnetAddress)) {
      const subnetInfo = map.get(subnetAddress);
      subnetInfo!.serverInfos.push(serverInfo);
      subnetInfo!.failurePeriods = calcPeriodsOverlap(
        subnetInfo!.failurePeriods,
        serverInfo.failurePeriods
      );
    } else {
      map.set(subnetAddress, {
        address: subnetAddress,
        serverInfos: [serverInfo],
        failurePeriods: serverInfo.failurePeriods,
      });
    }
  });

  return map;
};

// 設問4を実行する
export const solveQuestion4 = async (filePath: string, N: number) => {
  const textData = await readLog(filePath);
  const records = convertLogToRecords(textData);
  const serverInfos = getServerInfos(records, N - 1);
  printSubnetFailurePeriods(getSubnetInfos(serverInfos));
};

// 設問4を実行するコマンドを登録する
export const registerSolve4 = () => {
  program
    .command("solve4")
    .description("設問4のプログラムを実行します")
    .argument("[filePath]", "読み込むログのパス", "logs/sample.log")
    .option("-N, --num [number]", "何回タイムアウトしたら故障とみなすか", "2")
    .action(async (filePath, options) => {
      try {
        await solveQuestion4(path.resolve(filePath), +options.num);
      } catch {
        console.error("引数に指定したファイルは存在しません");
      }
    });
};
