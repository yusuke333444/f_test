import path from "path";
import { program } from "commander";

import { readLog } from "../utils/log-reader";
import {
  getRecordsMap,
  LogRecord,
  convertLogToRecords,
} from "../utils/log-parser";
import {
  printFailurePeriods,
  Period,
  ServerInfo,
} from "../utils/result-printer";

const isFailureEnd = (
  failureStartDate: Date | null,
  timeOutCount: number,
  allowedRetries: number
): failureStartDate is Date => {
  return failureStartDate != null && timeOutCount > allowedRetries;
};

// ログの情報から故障期間を導出する
const calcFailurePeriods = (
  records: LogRecord[],
  allowedRetries = 0
): Period[] => {
  if (records.every((record) => !record.ping.timeOut)) {
    return [];
  }

  let failureStartDate: Date | null = null;

  // 何回連続でタイムアウトしたか
  let timeOutCount = 0;
  const failurePeriods: Period[] = [];
  records.forEach((record) => {
    if (record.ping.timeOut) {
      timeOutCount += 1;
      if (failureStartDate == null) {
        // 故障と判定された際の開始時刻として使う
        failureStartDate = record.dateTime;
      }
      return;
    }

    if (isFailureEnd(failureStartDate, timeOutCount, allowedRetries)) {
      // 故障の終了
      failurePeriods.push({
        start: failureStartDate,
        end: record.dateTime,
        type: "failure",
      });
      failureStartDate = null;
    }
    // 故障とみなされる前にpingの応答ありの場合
    timeOutCount = 0;
  });

  if (isFailureEnd(failureStartDate, timeOutCount, allowedRetries)) {
    // ログの最後まで故障が未終了
    failurePeriods.push({
      start: failureStartDate,
      type: "failure",
    });
  }

  return failurePeriods;
};

// サーバ毎の記録や故障期間をまとめたマップを返却する
export const getServerInfos = (
  records: LogRecord[],
  allowedRetries = 0
): Map<string, ServerInfo> => {
  const recordsForEachDestination = getRecordsMap(records);
  const serverInfoMap = new Map<string, ServerInfo>();

  recordsForEachDestination.forEach((serverRecords, address) => {
    const failurePeriods = calcFailurePeriods(serverRecords, allowedRetries);
    serverInfoMap.set(address, {
      address,
      records: serverRecords,
      failurePeriods,
    });
  });

  return serverInfoMap;
};

// 設問2を実行する
export const solveQuestion2 = async (filePath: string, N: number) => {
  const textData = await readLog(filePath);
  const records = convertLogToRecords(textData);
  printFailurePeriods(getServerInfos(records, N - 1));
};

// 設問2を実行するコマンドを登録する
export const registerSolve2 = () => {
  program
    .command("solve2")
    .description("設問2のプログラムを実行します")
    .argument("[filePath]", "読み込むログのパス", "logs/sample.log")
    .option("-N, --num [number]", "何回タイムアウトしたら故障とみなすか", "2")
    .action(async (filePath, options) => {
      try {
        await solveQuestion2(path.resolve(filePath), +options.num);
      } catch {
        console.error("引数に指定したファイルは存在しません");
      }
    });
};
