import { program } from "commander";

import { registerSolve1 } from "./questions/question1";
import { registerSolve2 } from "./questions/question2";
import { registerSolve3 } from "./questions/question3";
import { registerSolve4 } from "./questions/question4";

const main = async (): Promise<void> => {
  // 各設問を実行するコマンドを登録する
  registerSolve1();
  registerSolve2();
  registerSolve3();
  registerSolve4();

  // コマンド実行する
  await program.parseAsync(process.argv);
};

main();
