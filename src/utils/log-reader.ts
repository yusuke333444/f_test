import fs from "fs";

// 指定したパスのログファイルを読み込み文字列として返す
export const readLog = async (filePath: string): Promise<string> => {
  return await fs.promises.readFile(filePath, { encoding: "utf-8" });
};
