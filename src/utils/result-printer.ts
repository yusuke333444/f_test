import { format, differenceInSeconds } from "date-fns";

import { LogRecord } from "./log-parser";

export type PeriodType = "failure" | "overloading";

export type Period = {
  start: Date;
  end?: Date;
  type: PeriodType;
};

export type ServerInfo = {
  address: string;
  records: LogRecord[];
  failurePeriods: Period[];
};

export type ServerInfo3 = ServerInfo & {
  overloadingPeriods: Period[];
};

export type SubnetInfo = {
  address: string;
  serverInfos: ServerInfo[];
  failurePeriods: Period[];
};

export const printServerPeriods = (
  periods: Period[],
  address: string
): boolean => {
  const FORMAT_STR = "yyyy/MM/dd HH:mm:ss";
  if (periods.length === 0) {
    return false;
  }
  console.log(`=== ${address} ===`);
  periods.forEach((period) => {
    const start = format(period.start, FORMAT_STR);
    const end = period.end == null ? "-" : format(period.end, FORMAT_STR);
    const status = period.type === "failure" ? "故障" : "過負荷";
    console.log(`START: ${start}, END: ${end}, STATUS: ${status}`);
  });
  console.log("\n");
  return true;
};

export const printFailurePeriods = (serverInfos: Map<string, ServerInfo>) => {
  console.log("故障サーバとその故障期間一覧");

  let failureServerExists = false;
  serverInfos.forEach((serverInfo, address) => {
    const result = printServerPeriods(serverInfo.failurePeriods, address);
    if (result) {
      failureServerExists = true;
    }
  });
  if (!failureServerExists) {
    console.log("=========");
    console.log("故障サーバなし\n");
  }
  console.log("=== 以上 ===\n");
};

export const printSubnetFailurePeriods = (
  subnetInfos: Map<string, SubnetInfo>
) => {
  console.log("故障サブネットとその故障期間一覧");

  let failureSubnetExists = false;
  subnetInfos.forEach((subnetInfo, address) => {
    const result = printServerPeriods(subnetInfo.failurePeriods, address);
    if (result) {
      failureSubnetExists = true;
    }
  });
  if (!failureSubnetExists) {
    console.log("=========");
    console.log("故障サブネットなし\n");
  }
  console.log("=== 以上 ===\n");
};

export const printAnomalousPeriods = (
  serverInfos: Map<string, ServerInfo3>
) => {
  console.log("故障中または過負荷状態であったサーバとその状態だった期間一覧");

  let anomalousServerExists = false;
  serverInfos.forEach(({ failurePeriods, overloadingPeriods }, address) => {
    const combinedPeriods = failurePeriods.concat(overloadingPeriods);
    const sortedPeriods = combinedPeriods.slice().sort((p1, p2) => {
      return differenceInSeconds(p1.start, p2.start);
    });
    const result = printServerPeriods(sortedPeriods, address);
    if (result) {
      anomalousServerExists = true;
    }
  });

  if (!anomalousServerExists) {
    console.log("=========");
    console.log("故障中または過負荷状態であったサーバなし\n");
  }

  console.log("=== 以上 ===\n");
};
