import { parse } from "date-fns";

export type SucceedPingResponse = {
  timeOut: false;
  responseTimeMs: number;
};

type FailedPingResponse = {
  timeOut: true;
};

type PingResponse = SucceedPingResponse | FailedPingResponse;

export type LogRecord = {
  dateTime: Date;
  destination: {
    address: string;
    prefix: number;
  };
  ping: PingResponse;
};

export type LogRecord3 = LogRecord & {
  meanResTime: number | null;
};

const formatPingResponse = (data: string): PingResponse => {
  if (data === "-") {
    return {
      timeOut: true,
    };
  }
  return {
    timeOut: false,
    responseTimeMs: +data,
  };
};

const convertLineToRecord = (lineText: string): LogRecord => {
  const [dateTime, addressWithPrefix, result] = lineText.split(",");
  const [address, prefix] = addressWithPrefix.split("/");
  const ping = formatPingResponse(result);
  return {
    dateTime: parse(dateTime, "yyyyMMddHHmmss", new Date()),
    destination: {
      address,
      prefix: +prefix,
    },
    ping,
  };
};

// ログファイルの内容を行毎の情報をもつオブジェクトの配列に変換する
export const convertLogToRecords = (textData: string): LogRecord[] => {
  const lineTexts = textData.split("\n").filter((t) => t != "");
  return lineTexts.map(convertLineToRecord);
};

const sum = (nums: number[]): number => {
  return nums.reduce((prev, current) => prev + current);
};

const calcMean = (...nums: number[]): number => {
  return sum(nums) / nums.length;
};

// 直近m回の平均応答時間の情報が付加されたレコードに変換する
const recordsToRecord3s = (records: LogRecord[], m: number): LogRecord3[] => {
  return records.map((record, index, orgRecords) => {
    if (index < m - 1) {
      return { ...record, meanResTime: null };
    }

    const prevRecords = orgRecords.slice(index - m + 1, index + 1);
    if (prevRecords.some((record) => record.ping.timeOut)) {
      return { ...record, meanResTime: null };
    }
    const responseTimes = prevRecords.map((record) => {
      return (record.ping as SucceedPingResponse).responseTimeMs;
    });

    const meanResTime = calcMean(...responseTimes);
    return { ...record, meanResTime };
  });
};

export const getRecordsMap = (
  records: LogRecord[]
): Map<string, LogRecord[]> => {
  const map = new Map<string, LogRecord[]>();
  records.forEach((record) => {
    const address = record.destination.address;
    if (map.has(address)) {
      const arr = map.get(address);
      arr!.push(record);
    } else {
      map.set(address, [record]);
    }
  });

  return map;
};

export const getRecord3sMap = (
  records: LogRecord[],
  m: number
): Map<string, LogRecord3[]> => {
  const map = new Map<string, LogRecord3[]>();
  getRecordsMap(records).forEach((serverRecords, address) => {
    map.set(address, recordsToRecord3s(serverRecords, m));
  });
  return map;
};
