const { addSeconds, format } = require("date-fns");

const YEAR = 2020;
const MONTH = 10;
const DAY = 19;
const HOUR = 13;
const MIN = 31;
const SEC = 24;

const TEST1 = {
  "10.20.30.1/16": [3, 2, 10, 3, 5], // 全部正常パターン
  "10.20.30.2/16": [3, 2, 10, null, 5], // 単発タイムアウト復旧パターン
  "192.168.1.1/24": [3, 2, 10, null, null], // 復旧せずパターン
  "192.168.1.2/24": [3, 2, null, null, 8], // 複数タイムアウト復旧パターン
  "192.168.1.3/24": [3, null, 10, null, 5], // 複数故障パターン
};

const TEST2 = {
  "10.20.30.1/16": [3, 2, 10, 3, 5], // 全部正常パターン
  "10.20.30.2/16": [3, 2, 10, null, 5], // N回超えないパターン
  "192.168.1.1/24": [3, 2, 10, null, null], // タイムアウト2回、普及せず
  "192.168.1.2/24": [3, 2, 10, 3, null], // N回超えないパターン、復旧せず
  "192.168.1.3/24": [null, null, 10, null, null], // 複数故障パターン
};

const TEST3 = {
  "10.20.30.1/16": [3, 2, 10, 3, 5], // 全部正常パターン
  "10.20.30.2/16": [30, 30, 100, 10, 5], // 単発過負荷パターン
  "192.168.1.1/24": [30, 30, 100, 40, 50], // 復旧せずパターン
  "192.168.1.2/24": [30, 30, 100, 30, 8], // 複数過負荷復旧パターン
  "192.168.1.3/24": [30, 30, 100, null, null], // 故障混合パターン
};

const TEST4 = {
  // 故障期間に重複がないパターン
  "10.20.30.1/16": [null, null, 10, 3, 5], // 13:31:24 - 13:31:56
  "10.20.30.2/16": [3, 2, 10, null, null], // 13:32:16 -

  // 故障期間に重複があるパターン
  "192.168.1.1/24": [3, null, null, null, null], // 13:31:48 ~
  "192.168.1.2/24": [3, null, null, null, 8], // 13:31:52 - 13:32:40
  "192.168.1.3/24": [3, 2, null, null, 5], // 13:32:12 - 13:32:44
};

const resTimes = TEST4;

const baseDate = new Date(YEAR, MONTH, DAY, HOUR, MIN, SEC);
const J_MAX = Object.keys(resTimes).length;
for (let i = 0; i < 5; i++) {
  for (let j = 0; j < J_MAX; j++) {
    const FORMAT_STR = "yyyyMMddHHmmss";
    const dt = format(addSeconds(baseDate, (i * 4 + j) * 4), FORMAT_STR);
    const address = Object.keys(resTimes)[j];
    const resTime = resTimes[address][i];
    console.log([dt, address, resTime ? resTime : "-"].join());
  }
}
